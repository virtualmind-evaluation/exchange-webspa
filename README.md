# ExchangeWebspa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Considerations future improvements

- Validations should be improved adding the errors on each control when it is dirty or the form was submitted instead of disable the button
- UserId must not be sent using the form. Authentication should be implemented on the backend and a login page should be added
- Styles can be improved 
- Unit tests can be added 
