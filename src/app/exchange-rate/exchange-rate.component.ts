import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromExchangeRate from '../modules/root-store/exchange-rate';
import { Observable } from 'rxjs';
import { Currency } from '../models/currency';
import { CurrencyRate, Rate } from '../models/currency-rate';

@Component({
  selector: 'app-exchange-rate',
  templateUrl: './exchange-rate.component.html',
  styleUrls: ['./exchange-rate.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExchangeRateComponent implements OnInit {
  rates$!: Observable<CurrencyRate[]>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(fromExchangeRate.load());

    this.rates$ = this.store.select(fromExchangeRate.selectRates);
  }
}
