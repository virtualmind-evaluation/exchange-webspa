import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { reload } from '../modules/root-store/exchange-rate';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuoteComponent {
  constructor(private store: Store) {}

  loadRates() {
    this.store.dispatch(reload());
  }
}
