import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Currency } from '../models/currency';
import { Observable } from 'rxjs';
import { Rate } from '../models/currency-rate';
import { environment } from 'src/environments/environment';
import { PurchaseTransaction } from '../models/purchase-transaction';

@Injectable({ providedIn: 'root' })
export class ExchangeService {
  constructor(private http: HttpClient) {}

  getRates(currency: Currency): Observable<Rate> {
    return this.http.get<Rate>(
      `${environment.apiUrl}/exchange/${currency.toString()}`
    );
  }

  purchaseTransaction(transaction: PurchaseTransaction): Observable<number> {
    return this.http.post<number>(
      `${environment.apiUrl}/exchange/`,
      transaction
    );
  }
}
