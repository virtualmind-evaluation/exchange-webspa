import { PurchaseTransaction } from 'src/app/models/purchase-transaction';

export interface State {
  transaction?: PurchaseTransaction;
  amount?: number;
  exceeded?: boolean;
}

export const initialState: State = {};
