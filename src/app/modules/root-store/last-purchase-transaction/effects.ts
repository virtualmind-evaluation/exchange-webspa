import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap, throwError } from 'rxjs';
import { ExchangeService } from 'src/app/services/exchange.service';
import * as purchaseRequestActions from './actions';
import * as loadingActions from '../loading/actions';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class lastPurchaseTransactionEffects {
  purchase$ = createEffect(() => {
    return this.action$.pipe(
      ofType(purchaseRequestActions.purchaseRequest),
      switchMap((request) =>
        this.exchangeService.purchaseTransaction(request.transaction).pipe(
          catchError((err) => {
            if (err instanceof HttpErrorResponse) {
              if (
                err.status === 400 &&
                err.error.detail.startsWith(
                  'Amount of transaction exceeds monthly transaction limit. '
                )
              ) {
                return of(null);
              }
            }

            return throwError(() => err);
          })
        )
      ),
      map((x) =>
        x == null
          ? purchaseRequestActions.purchaseRequestExceeded()
          : purchaseRequestActions.purchaseRequestLoaded(x as number)
      )
    );
  });

  setupLoadingStart$ = createEffect(() => {
    return this.action$.pipe(
      ofType(purchaseRequestActions.purchaseRequest),
      map(() => loadingActions.load())
    );
  });

  setupLoadingEnd$ = createEffect(() => {
    return this.action$.pipe(
      ofType(
        purchaseRequestActions.purchaseRequestLoaded,
        purchaseRequestActions.purchaseRequestExceeded
      ),
      map(() => loadingActions.loaded())
    );
  });

  constructor(
    private action$: Actions,
    private exchangeService: ExchangeService
  ) {}
}
