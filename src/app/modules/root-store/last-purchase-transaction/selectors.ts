import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './state';

export const featureKey = 'lastPurchaseTransaction';
export const selectFeature = createFeatureSelector<State>(featureKey);

export const selectTransaction = createSelector(
  selectFeature,
  (state: State) => state.transaction
);

export const selectAmount = createSelector(
  selectFeature,
  (state: State) => state.amount
);
