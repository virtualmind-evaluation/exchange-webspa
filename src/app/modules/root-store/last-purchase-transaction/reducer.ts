import { createReducer, on, Action } from '@ngrx/store';
import * as transactionActions from './actions';
import { initialState, State } from './state';

const rateReducer = createReducer<State>(
  initialState,
  on(
    transactionActions.purchaseRequest,
    (_state, { transaction }): State => ({ transaction })
  ),
  on(
    transactionActions.purchaseRequestLoaded,
    (state, { amount }): State => ({ ...state, amount })
  ),
  on(
    transactionActions.purchaseRequestExceeded,
    (state): State => ({ ...state, exceeded: true })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return rateReducer(state, action);
}
