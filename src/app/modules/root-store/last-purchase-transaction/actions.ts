import { createAction } from '@ngrx/store';
import { PurchaseTransaction } from 'src/app/models/purchase-transaction';

export const purchaseRequest = createAction(
  '[Purchase] request',
  (transaction: PurchaseTransaction) => ({ transaction })
);
export const purchaseRequestLoaded = createAction(
  '[Purchase] request loaded',
  (amount: number) => ({ amount })
);

export const purchaseRequestExceeded = createAction(
  '[Purchase] request exceeded'
);
