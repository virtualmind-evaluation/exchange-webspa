import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromExchangeRate from './exchange-rate';
import * as fromLastPurchaseTransaction from './last-purchase-transaction';
import * as fromLoading from './loading';
import { EffectsModule } from '@ngrx/effects';
import { NgxSpinnerModule } from 'ngx-spinner';

function throwIfAlreadyLoaded(parentModule: object, moduleName: string) {
  if (parentModule) {
    throw new Error(
      `${moduleName} has already been loaded. Import ${moduleName} in the AppModule only.`
    );
  }
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    StoreModule.forRoot(
      {
        [fromExchangeRate.featureKey]: fromExchangeRate.reducer,
        [fromLoading.featureKey]: fromLoading.reducer,
        [fromLastPurchaseTransaction.featureKey]:
          fromLastPurchaseTransaction.reducer,
      },
      {
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true,
        },
      }
    ),
    EffectsModule.forRoot([
      fromExchangeRate.RateEffects,
      fromLoading.LoadingEffects,
      fromLastPurchaseTransaction.lastPurchaseTransactionEffects,
    ]),
  ],
})
export class RootStoreModule {
  constructor(@Optional() @SkipSelf() parentModule: RootStoreModule) {
    throwIfAlreadyLoaded(parentModule, 'RootStoreModule');
  }
}
