import { CurrencyRate } from 'src/app/models/currency-rate';

export interface State {
  rates: CurrencyRate[];
  hasLoaded: boolean;
}

export const initialState: State = {
  hasLoaded: false,
  rates: [],
};
