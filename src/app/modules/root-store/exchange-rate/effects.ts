import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  Observable,
  filter,
  from,
  map,
  mergeMap,
  switchMap,
  toArray,
  withLatestFrom,
} from 'rxjs';
import { CURRENCIES } from 'src/app/models/currency';
import { CurrencyRate } from 'src/app/models/currency-rate';
import { ExchangeService } from 'src/app/services/exchange.service';
import * as exchangeRateActions from './actions';
import * as loadingActions from '../loading/actions';
import * as exchangeRateSelectors from './selectors';
import { Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class RateEffects {
  rates$: Observable<CurrencyRate[]> = from(CURRENCIES.map((x) => x.code)).pipe(
    mergeMap((currency) =>
      this.exchangeService
        .getRates(currency)
        .pipe(map((rate) => ({ currency, rate })))
    ),
    toArray()
  );

  loadExchangeRate$ = createEffect(() => {
    return this.action$.pipe(
      ofType(exchangeRateActions.load),
      withLatestFrom(this.store.select(exchangeRateSelectors.selectHasLoaded)),
      filter(([_, hasLoaded]) => !hasLoaded),
      map(() => exchangeRateActions.reload())
    );
  });

  reloadExchangeRate$ = createEffect(() => {
    return this.action$.pipe(
      ofType(exchangeRateActions.reload),
      switchMap(() => this.rates$),
      map((x) => exchangeRateActions.loaded(x))
    );
  });

  setupLoadingStart$ = createEffect(() => {
    return this.action$.pipe(
      ofType(exchangeRateActions.reload),
      map(() => loadingActions.load())
    );
  });

  setupLoadingEnd$ = createEffect(() => {
    return this.action$.pipe(
      ofType(exchangeRateActions.loaded),
      map(() => loadingActions.loaded())
    );
  });

  constructor(
    private action$: Actions,
    private store: Store,
    private exchangeService: ExchangeService
  ) {}
}
