import { createReducer, on, Action } from '@ngrx/store';
import * as rateActions from './actions';
import { initialState, State } from './state';

const rateReducer = createReducer<State>(
  initialState,
  on(
    rateActions.reload,
    (): State => ({
      rates: [],
      hasLoaded: false,
    })
  ),
  on(
    rateActions.loaded,
    (_state, { rates }): State => ({
      rates,
      hasLoaded: true,
    })
  ),
  on(rateActions.unload, (): State => initialState)
);

export function reducer(state: State | undefined, action: Action) {
  return rateReducer(state, action);
}
