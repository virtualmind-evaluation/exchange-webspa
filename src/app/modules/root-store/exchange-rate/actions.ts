import { createAction } from '@ngrx/store';
import { CurrencyRate } from 'src/app/models/currency-rate';

export const load = createAction('[ExchangeRate] load');
export const reload = createAction('[ExchangeRate] reload');
export const loaded = createAction(
  '[ExchangeRate] loaded',
  (rates: CurrencyRate[]) => ({ rates })
);
export const unload = createAction('[ExchangeRate] unload');
