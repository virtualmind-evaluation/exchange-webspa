import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './state';

export const featureKey = 'exchangeRate';
export const selectFeature = createFeatureSelector<State>(featureKey);

export const selectRates = createSelector(
  selectFeature,
  (state: State) => state.rates
);

export const selectHasLoaded = createSelector(
  selectFeature,
  (state: State) => state.hasLoaded
);
