import { createReducer, on, Action } from '@ngrx/store';
import * as loadActions from './actions';
import { initialState, State } from './state';

const loadReducer = createReducer<State>(
  initialState,
  on(
    loadActions.load,
    (state): State => ({
      loadingCount: state.loadingCount + 1,
    })
  ),
  on(
    loadActions.loaded,
    (state): State => ({
      loadingCount: state.loadingCount - 1,
    })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return loadReducer(state, action);
}
