export interface State {
  loadingCount: number;
}

export const initialState: State = {
  loadingCount: 0,
};
