import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './state';

export const featureKey = 'loading';
export const selectFeature = createFeatureSelector<State>(featureKey);

export const selectLoading = createSelector(
  selectFeature,
  (state: State) => state.loadingCount > 0
);
