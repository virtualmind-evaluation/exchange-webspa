import { Injectable } from '@angular/core';
import { createEffect } from '@ngrx/effects';
import { tap } from 'rxjs';
import * as loadingSelectors from './selectors';
import { Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class LoadingEffects {
  showSpinner = createEffect(
    () => {
      return this.store.select(loadingSelectors.selectLoading).pipe(
        tap((x) => {
          if (x) {
            this.spinner.show();
          } else {
            this.spinner.hide();
          }
        })
      );
    },
    { dispatch: false }
  );

  constructor(private store: Store, private spinner: NgxSpinnerService) {}
}
