import { createAction } from '@ngrx/store';

export const load = createAction('[Application] load');
export const loaded = createAction('[Application] loaded');
