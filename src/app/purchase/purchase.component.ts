import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CURRENCIES } from '../models/currency';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromLastPurchaseTransaction from '../modules/root-store/last-purchase-transaction';

@Component({
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PurchaseComponent implements OnInit {
  currencies = CURRENCIES;
  form!: FormGroup;
  lastTransaction$!: Observable<fromLastPurchaseTransaction.State>;

  constructor(fb: FormBuilder, private store: Store) {
    this.form = fb.group({
      userId: [null, Validators.required],
      currencyCode: [null, Validators.required],
      amount: [null, Validators.required],
    });
  }

  ngOnInit(): void {
    this.lastTransaction$ = this.store.select(
      fromLastPurchaseTransaction.selectFeature
    );
  }

  clear() {
    this.form.reset();
  }

  submit() {
    if (this.form.invalid) return;

    const request = this.form.value;
    this.store.dispatch(fromLastPurchaseTransaction.purchaseRequest(request));
    this.clear();
  }
}
