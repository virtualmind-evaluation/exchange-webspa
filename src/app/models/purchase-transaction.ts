import { CurrencyType } from './currency';

export interface PurchaseTransaction {
  userId: string;
  currencyCode: CurrencyType;
  amount: number;
}
