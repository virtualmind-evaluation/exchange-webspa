import { Currency } from './currency';

export interface Rate {
  buy: number;
  sell: number;
}

export interface CurrencyRate {
  currency: Currency;
  rate: Rate;
}
