export enum Currency {
  USD = 'USD',
  BRL = 'BRL',
}

export const CURRENCIES = [
  { name: 'Dollar', code: Currency.USD },
  { name: 'Real', code: Currency.BRL },
];

export type CurrencyType = Currency.USD | Currency.BRL;
